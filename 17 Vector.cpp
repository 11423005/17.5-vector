﻿#include <iostream>
#include <cmath>

class Vector 
{
private:
    double x;
    double y;
    double z; // переменные в приватной видимости

public:
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void getValues()  
    {
        std::cout << "x: " << x << ", y: " << y << ", z: " << z << std::endl;
    }

    double getLength()  
    {
        return std::sqrt(x * x + y * y + z * z); // расчет длины (модуля) вектора (корень из суммы квадратов координат)
    }
};

int main() 
{
    Vector vec(2.0, 3.0, 4.0); // показ данных через публичный метод
    Vector vec1(6.3, 6.3, 6.2);

    std::cout << "Values of vector: ";    // Вывод значений вектора
    vec.getValues();
    std::cout << "Length of vector: " << vec.getLength() << '\n'; // Вычисление и вывод длины (модуля) вектора1;
    vec1.getValues();  
    std::cout << "Length of vector: " << vec1.getLength() << std::endl; // Вычисление и вывод длины (модуля) вектора2;
        

    return 0;
}